# task22_bind

Практическая работа: настраиваем свой кеширующий днс (мастер/слейв) со своей локальной зоной

```
Домашнее задание
настраиваем split-dns
взять стенд https://github.com/erlong15/vagrant-bind
добавить еще один сервер client2
завести в зоне dns.lab
имена
web1 - смотрит на клиент1
web2 смотрит на клиент2

завести еще одну зону dnsnew.lab
завести в ней запись
www - смотрит на обоих клиентов

настроить split-dns
клиент1 - видит обе зоны, но в зоне dns.lab только web1

клиент2 видит только dns.lab

*) настроить все без выключения selinux
Критерии оценки: 4 - основное задание сделано, но есть вопросы
5 - сделано основное задание
6 - выполнено задания со звездочкой
```

# Результат
Результатом выполнения домашнего задания является Vagrant файл, который средствами ansible provisioning подготавливает следующий стенд:
```
Первичный DNS сервер ns01 (192.168.50.10)
Вторичный DNS сервер ns02 (192.168.50.11)
Клиент client01 (192.168.50.15)
Клиент client02 (192.168.50.16)
```
Проверка на клиенте 1
```
// Проверка разрешения web01 на master и slave
[vagrant@client01 ~]$ dig +short web01.dns.lab @192.168.50.10
192.168.50.15
[vagrant@client01 ~]$ dig +short web01.dns.lab @192.168.50.11
192.168.50.15

// Проверка разрешения web02 на master и slave
[vagrant@client01 ~]$ dig +short web02.dns.lab @192.168.50.10
[vagrant@client01 ~]$ dig +short web02.dns.lab @192.168.50.11

// Проверка разрешения домена dnsnew на master и slave
[vagrant@client01 ~]$ dig +short www.dnsnew.lab @192.168.50.10
192.168.50.15
192.168.50.16
[vagrant@client01 ~]$ dig +short www.dnsnew.lab @192.168.50.11
192.168.50.15
192.168.50.16
```
Проверка на клиенте 2
```
// Проверка разрешения web01 на master и slave
[vagrant@client02 ~]$ dig +short web01.dns.lab @192.168.50.10
192.168.50.15
[vagrant@client02 ~]$ dig +short web01.dns.lab @192.168.50.11
192.168.50.15

// Проверка разрешения web2 на master и slave
[vagrant@client02 ~]$ dig +short web02.dns.lab @192.168.50.10
192.168.50.16
[vagrant@client02 ~]$ dig +short web02.dns.lab @192.168.50.11
192.168.50.16

// Проверка разрешения домена dnsnew на master и slave
[vagrant@client02 ~]$ dig +short www.dnsnew.lab @192.168.50.10
[vagrant@client02 ~]$ dig +short www.dnsnew.lab @192.168.50.11
```
SELinux выключил
```
На сервере ns01

[root@ns01 vagrant]# getsebool -a | grep named
named_tcp_bind_http_port --> off
named_write_master_zones --> on
